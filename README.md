
# Kapta

## Introducción

Este es un codigo desarrollado para Kapta, donde se desarrolló un gestor de tareas, que se sincorniza con FreedCamop

La plataforma está desarrollada en PHP, con un entorno de desarrollo llamado [Laravel](https://laravel.com/)


## Requerimientos Básicos

Para poder instalar y hacer funcionar el código se requiere

* PHP >= 7.2.0
* BCMath PHP Extension
* Ctype PHP Extension
* JSON PHP Extension
* Mbstring PHP Extension
* OpenSSL PHP Extension
* PDO PHP Extension
* Tokenizer PHP Extension
* XML PHP Extension

## Instalación

* Si se tiene una distribución XAMP, sólo es necesario copiar y pegar todo el repositorio en la respectiva carpeta. 
Se debe tener en cuenta que la carpeta pública se llama public

* Otra opción mucho más rapida, es usar [Laragon](https://laragon.org/). Este  software ya tiene todo lo necesario para ejecutar Laravel. 
E igualmente habría que copiar y pegar la carpeta en C//:laragon/www/ 

## Consideraciones

Se sube al repositorio la carpeta Vendor, la cual no debe ser necesaria si se cuenta con Laravel instalado en el ordenador. Solo es necesario crear un nuevo proyecto y reemplazar los otros archivos
