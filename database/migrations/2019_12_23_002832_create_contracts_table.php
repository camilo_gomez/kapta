<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContractsTable extends Migration {

	public function up()
	{
		Schema::create('contracts', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('service_id')->unsigned()->nullable();
			$table->integer('enterprise_id')->unsigned()->nullable();
			$table->integer('client_id')->unsigned()->nullable();
                        $table->integer('status')->unsigned();
			$table->timestamps();
			
		});
	}

	public function down()
	{
		Schema::drop('contracts');
	}
}