<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEnterpriseServiceTable extends Migration {

	public function up()
	{
		Schema::create('enterprise_service', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('enterprise_id')->unsigned()->nullable();
			$table->integer('service_id')->unsigned()->nullable();
		});
	}

	public function down()
	{
		Schema::drop('enterprise_service');
	}
}