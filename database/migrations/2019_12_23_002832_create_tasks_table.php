<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTasksTable extends Migration {

	public function up()
	{
		Schema::create('tasks', function(Blueprint $table) {
			$table->increments('id');
			$table->string('title');
			$table->bigInteger('fc_id')->unique()->unsigned()->nullable();
			$table->integer('project_id')->unsigned()->nullable();
			$table->integer('priority')->unsigned()->index();
			$table->integer('assigned_to')->unsigned()->nullable();
			$table->integer('status')->unsigned();
			$table->datetime('started_at')->nullable();
			$table->datetime('finished_at')->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('tasks');
	}
}