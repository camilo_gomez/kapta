<?php

use Illuminate\Database\Seeder;
use App\Project;
use App\Task;

class ProjectsTaskSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        Project::truncate();
        Task::truncate();
        


    }

}
