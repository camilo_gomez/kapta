<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\EnterpriseService;

class EnterpriseServiceTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        EnterpriseService::truncate();
        EnterpriseService::create([
            'enterprise_id' => '1',
            'service_id' => '1',
        ]);
        EnterpriseService::create([
            'enterprise_id' => '1',
            'service_id' => '2',
        ]);
        EnterpriseService::create([
            'enterprise_id' => '1',
            'service_id' => '4',
        ]);
        EnterpriseService::create([
            'enterprise_id' => '2',
            'service_id' => '2',
        ]);
        EnterpriseService::create([
            'enterprise_id' => '2',
            'service_id' => '4',
        ]);
        EnterpriseService::create([
            'enterprise_id' => '2',
            'service_id' => '5',
        ]);
    }

}
