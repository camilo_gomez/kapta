<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Client;

class ClientsTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        Client::truncate();
        Client::create([
            'name' => 'BMW',
        ]);
        Client::create([
            'name' => 'KIA',
        ]);
        Client::create([
            'name' => 'MINI',
        ]);
        Client::create([
            'name' => 'VOLVO',
        ]);


    }

}
