<?php

use Illuminate\Database\Seeder;
use App\Contract;

class ContractsTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        Contract::truncate();
        Contract::create([
            'service_id' => '1',
            'enterprise_id' => '1',
            'client_id' => '1',
            'status' => Contract::STATUS_IN_PROGRESS
        ]);
        Contract::create([
            'service_id' => '2',
            'enterprise_id' => '2',
            'client_id' => '2',
            'status' => Contract::STATUS_IN_PROGRESS
        ]);
         Contract::create([
            'service_id' => '3',
            'enterprise_id' => '1',
            'client_id' => '4',
            'status' => Contract::STATUS_IN_PROGRESS
        ]);
          Contract::create([
            'service_id' => '4',
            'enterprise_id' => '2',
            'client_id' => '3',
            'status' => Contract::STATUS_IN_PROGRESS
        ]);
           Contract::create([
            'service_id' => '5',
            'enterprise_id' => '1',
            'client_id' => '1',
            'status' => Contract::STATUS_IN_PROGRESS
        ]);
            Contract::create([
            'service_id' => '1',
            'enterprise_id' => '2',
            'client_id' => '3',
            'status' => Contract::STATUS_IN_PROGRESS
        ]);
             Contract::create([
            'service_id' => '2',
            'enterprise_id' => '1',
            'client_id' => '4',
            'status' => Contract::STATUS_IN_PROGRESS
        ]);
    }

}
