<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(EnterprisesTableSeeder::class);
         $this->call(ServicesTableSeeder::class);
         $this->call(ClientsTableSeeder::class);
         $this->call(ContractsTableSeeder::class);
         $this->call(EnterpriseServiceTableSeeder::class);
         $this->call(UsersTableSeeder::class);
         $this->call(ProjectsTaskSeeder::class);
    }
}
