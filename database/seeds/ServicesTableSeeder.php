<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Service;

class ServicesTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

      
        Service::truncate();
        Service::create([
            'name' => 'Administrativas',
        ]);
        Service::create([
            'name' => 'Soporte Técnico',
        ]);
        Service::create([
            'name' => 'Diseño Visual',
        ]);
        Service::create([
            'name' => 'Desarrollo de Software',
        ]);
        Service::create([
            'name' => 'Consultoría',
        ]);
        
    }

}
