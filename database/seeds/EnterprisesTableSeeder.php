<?php

use Illuminate\Database\Seeder;
use App\Enterprise;

class EnterprisesTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        Enterprise::truncate();
        Enterprise::create([
            'name' => 'GMBh',
        ]);
        Enterprise::create([
            'name' => 'K@PTA',
        ]);

    }

}
