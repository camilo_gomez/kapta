<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;

class UsersTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        User::truncate();
        User::create([
            'name' => 'Camilo Gomez',
            'email' => 'caegomezji@gmail.com',
            'fc_id' => '1742856',
            'password' => Hash::make('qwerty'),
        ]);


    }

}
