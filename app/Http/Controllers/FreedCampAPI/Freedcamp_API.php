<?php

namespace App\Http\Controllers\FreedCampAPI;

class Freedcamp_API {

    const F_DEV = false;
    const API_HOST = 'https://freedcamp.com';
    const API_HOST_DEV = 'http://freedcamp.bear.com';
    const METHOD_GET = 'GET';
    const METHOD_POST = 'POST';

    private $api_key = 'b5b4bea5bd07e654eeac0376d2de2b6a7931d1f1';
    private $api_secret = 'd8522c5414c1e94fe50af497721ab4875c996b01';

    function __construct() {
        
    }

    function request($path, $method = self::METHOD_GET, $payload = []) {
        $send_data = [];

        $send_data['api_key'] = $this->api_key;
        $send_data['timestamp'] = time();
        $send_data['hash'] = hash_hmac('sha1', $this->api_key . $send_data['timestamp'], $this->api_secret);

        $host = self::F_DEV ? self::API_HOST_DEV : self::API_HOST;
        $url = $host . $path;
        $ch = curl_init();

        $f_post = $method === self::METHOD_POST;

        $curl_config = array(
            CURLOPT_POST => $f_post,
            CURLOPT_RETURNTRANSFER => true,
        );

        if ($f_post) {
            $send_data['data'] = json_encode($payload);
            $curl_config[CURLOPT_POSTFIELDS] = $send_data;
        } else {
            if ($payload) {
                $send_data = array_merge($send_data, $payload);
            }

            $query = http_build_query($send_data);
            if ($query) {
                $url = $url . '?' . $query;
            }
        }

        $curl_config[CURLOPT_URL] = $url;

        curl_setopt_array($ch, $curl_config);

        if (self::F_DEV) {
            curl_setopt($ch, CURLOPT_VERBOSE, true);
        }

        $result = curl_exec($ch);
        
        $out_payload = null;
        $returned_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($returned_status >= 500) {
            e("Some internal error on server");
            return false;
        } else {
            $out_payload = json_decode($result, true);
            if ($out_payload === null) {
                $this->e('failed to decode string: ' . $result);
                return false;
            }
        }

        if ($returned_status != 200 && $out_payload) {
            e($returned_status . ': ' . $out_payload['msg']);
        }

        curl_close($ch);


        return $out_payload;
    }

    function getSession() {
        $url = '/api/v1/sessions/current';
        return $this->request($url);
    }

    function getMyId() {
        $session_data = $this->getSession();
        if (!$session_data) {
            exit('Something is wrong');
        }

        return $session_data['data']['user_id'];
    }

    function getTasks($assigned_to, $limit = 5, $offset = 0) {
        $url = '/api/v1/tasks';

        $params = [];
        $params['limit'] = $limit;
        $params['offset'] = $offset;
        $params['assigned_to_id'] = [$assigned_to];
        $params['sort'] = ['due_date' => 'desc'];

        $response = $this->request($url, self::METHOD_GET, $params);
        if (!empty($response['data']['tasks'])) {
            return $response['data']['tasks'];
        } else {
            return [];
        }
    }

    function getNotifications($project_id = null) {
        $url = '/api/v1/notifications';

        if ($project_id) {
            $url .= '/' . $project_id;
        }

        $params = [];
        $params['following'] = 1;

        $response = $this->request($url, self::METHOD_GET, $params);

        if (!empty($response['data']['notifications'])) {
            $this->e('My notifications are: ');
            e(implode(PHP_EOL, array_column($response['data']['notifications'], 'item_title')));
            return $response['data']['notifications'];
        } else {
            $this->e('No notifications found');
            return [];
        }
    }

    function getProjects() {
        $session_data = $this->getSession();
        if (!$session_data) {
            exit('Something is wrong');
        }
        if (!empty($session_data['data']['projects'])) {
            return $session_data['data']['projects'];
        } else {
            return [];
        }
    }

    function getGroups() {
        $session_data = $this->getSession();
        if (!$session_data) {
            exit('Something is wrong');
        }
        if (!empty($session_data['data']['groups'])) {
            return $session_data['data']['groups'];
        } else {
            return [];
        }
    }

    function postTask() {
        $url = '/api/v1/tasks';

        $data = array(
            'task_group_id' => 888, // real task group id
            'title' => 'ssdcscsdc',
            'extended_description' => '',
            'assigned_to_id' => 0,
            'priority' => 1,
            'project_id' => 616, // real project id
        );

        $result = $this->request($url, self::METHOD_POST, $data);
        return $result;
    }

// making output prettier and faster
    function e($arr) {
        if (is_array($arr) || is_object($arr)) {
            $t = print_r($arr, true);
        } else {
            $t = $arr;
        }
        $t = PHP_EOL . $t . PHP_EOL;
        echo($t);
    }

    function r($arr) {
        if (is_array($arr) || is_object($arr)) {
            $t = print_r($arr, true);
        } else {
            $t = $arr;
        }
        error_log($t);
    }

}
