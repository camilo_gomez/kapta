<?php

namespace App\Http\Controllers\Resource;

use App\Http\Controllers\Controller;
use App\Contract;
use App\Enterprise;
use App\Client;
use App\Service;
use Illuminate\Http\Request;
use App\Http\Requests\ContractCreateRequest;

class ContractController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $contracts = Contract::get();
        return view('models.contract.home', compact('contracts'));
    }

    public function changeStatus(Contract $contract, $status) {

        if ($status == Contract::STATUS_ENDED) {
            $contract->status = Contract::STATUS_ENDED;
        } else if ($status == Contract::STATUS_IN_PROGRESS) {
            $contract->status = Contract::STATUS_IN_PROGRESS;
        } else {
            $error = ["error"];
            return back()->withErrors($error);
        }
        $contract->save();
        return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $enterprises = Enterprise::get();
        $clients = Client::get();
        $services = Service::get();
        return view('models.contract.create', compact(
                        'enterprises', 'services', 'clients'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContractCreateRequest $request) {
        $contract = new Contract();
        $contract->enterprise_id = $request->input('enterprise');
        $contract->client_id = $request->input('client');
        $contract->service_id = $request->input('service');
        $contract->status = Contract::STATUS_IN_PROGRESS;
        $contract->save();
        return redirect()->route('contract.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function show(contract $contract) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function edit(contract $contract) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, contract $contract) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function destroy(contract $contract) {
        //
    }

}
