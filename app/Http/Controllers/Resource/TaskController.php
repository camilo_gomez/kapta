<?php

namespace App\Http\Controllers\Resource;

use App\Http\Controllers\Controller;
use App\Task;
use App\Project;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\TaskCreateRequest;

class TaskController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $tasks = new Task();
        if($request->input('project_id')){
            $tasks = $tasks->where('project_id' ,$request->input('project_id') );
        }
        if($request->input('user_id')){
            $tasks = $tasks->where('assigned_to' ,$request->input('user_id') );
        }
        
        if($request->input('enterprise_id')){
           $tasks = Task::select('tasks.*')                             
                ->join('projects' , 'projects.id' , '=' , 'tasks.project_id')
                ->join('contracts' , 'projects.contract_id' , '=' , 'contracts.id')
                ->where('contracts.enterprise_id',  $request->input('enterprise_id'));          
                      
        }
        
        $tasks = $tasks->get();
        
        return view('models.Task.home', compact('tasks'));
    }

    public function changeStatus(Task $task, $status) {

        if ($status == Task::STATUS_COMPLETED) {
            $task->finished_at = now();
            $task->status = Task::STATUS_COMPLETED;
        } else if ($status == Task::STATUS_IN_PROGRESS) {
            $task->started_at = now();
            $task->finished_at = null;
            $task->status = Task::STATUS_IN_PROGRESS;
        } else if ($status == Task::STATUS_NOT_STARTED) {
            $task->finished_at = null;
            $task->started_at = null;
            $task->status = Task::STATUS_NOT_STARTED;
        } else {
            $error = ["error"];
            return back()->withErrors($error);
        }
        $task->save();
        return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $projects = Project::get();
        $users = User::get();
         return view('models.task.create', compact('projects' , 'users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TaskCreateRequest $request) {
        
        $task = new Task();
        $task->title = $request->input('name');
        $task->project_id = $request->input('project');
        $task->priority = $request->input('priority');
        $task->assigned_to = $request->input('user');
        $task->status = Task::STATUS_NOT_STARTED;
        $task->save();
        return redirect()->route('task.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task) {
        //
    }

}
