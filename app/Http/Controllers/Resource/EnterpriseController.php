<?php

namespace App\Http\Controllers\Resource;

use App\Http\Controllers\Controller;
use App\enterprise;
use App\Contract;
use Illuminate\Http\Request;

class EnterpriseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $enterprises = enterprise::get();
        return view('models.enterprise.home' , compact('enterprises') );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\enterprise  $enterprise
     * @return \Illuminate\Http\Response
     */
    public function show(enterprise $enterprise)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\enterprise  $enterprise
     * @return \Illuminate\Http\Response
     */
    public function edit(enterprise $enterprise)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\enterprise  $enterprise
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, enterprise $enterprise)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\enterprise  $enterprise
     * @return \Illuminate\Http\Response
     */
    public function destroy(enterprise $enterprise)
    {
        //
    }
}
