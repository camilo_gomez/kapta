<?php

namespace App\Http\Controllers;

use App\Http\Controllers\FreedCampAPI\ProjectsAPIController;
use App\Http\Controllers\FreedCampAPI\TaskAPIController;
use App\Project;
use App\Task;
use App\User;

class SychronizeController extends Controller {

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function synchronize() {
        $this->synchronizeProjects();
        $this->synchronizeTasks();
        return redirect()->route('project.index');
    }

    public function synchronizeProjects() {
        $FCprojects = (new ProjectsAPIController())->index();
        $DBprojects = Project::get();
        foreach ($FCprojects as $fcproject) {
            $bandera = true;
            foreach ($DBprojects as $dbproject) {
                if ($dbproject->fc_id == $fcproject["project_id"]) {
                    $bandera = false;
                    $dbproject->title = $fcproject["project_name"];
                    $dbproject->description = $fcproject["project_description"];
                    $dbproject->save();
                }
            }

            if ($bandera) {
                $newProject = new Project();
                $newProject->title = $fcproject["project_name"];
                $newProject->description = $fcproject["project_description"];
                $newProject->fc_id = $fcproject["project_id"];
                $newProject->save();
            }
        }
    }

    public function synchronizeTasks() {
        $FCtasks = (new TaskAPIController())->index();
        $DBtasks = Task::get();
        foreach ($FCtasks as $fctask) {
            $bandera = true;
            foreach ($DBtasks as $dbtask) {
                if ($dbtask->fc_id == $fctask["id"]) {
                    $bandera = false;
                    $dbtask->title = $fctask["title"];
                    $dbtask->status = $fctask["status"];
                    $dbtask->priority = $fctask["priority"];

                    $user = User::where("fc_id", $fctask["assigned_to_id"])->first();
                    if ($user != null) {
                        $dbtask->assigned_to = $user->id;
                    }

                    $project = Project::where("fc_id", $fctask["project_id"])->first();
                    if ($project != null) {
                        $dbtask->project_id = $project->id;
                    }
                    $dbtask->finished_at = null;
                    $dbtask->save();
                }
            }

            if ($bandera) {
                    
                $newtask = new Task();
                $newtask->fc_id = $fctask["id"];
                $newtask->title = $fctask["title"];
                $newtask->status = $fctask["status"];
                $newtask->priority = $fctask["priority"];

                $user = User::where("fc_id", $fctask["assigned_to_id"])->first();
                if ($user != null) {
                    $newtask->assigned_to = $user->id;
                }

                $project = Project::where("fc_id", $fctask["project_id"])->first();
                if ($project != null) {
                    $newtask->project_id = $project->id;
                }

                $newtask->finished_at = null;
                $newtask->save();
            }
        }
    }

}
