<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contract extends Model {

    protected $table = 'contracts';
    public $timestamps = true;

    const STATUS_IN_PROGRESS = 1;
    const STATUS_ENDED = 0;
    
    protected $with = ['enterprise' , 'service' , 'client'];

    public function projects() {
        return $this->hasMany('App\Project', 'contract_id', 'id');
    }

    public function enterprise() {
        return $this->belongsTo('App\Enterprise', 'enterprise_id', 'id');
    }

    public function service() {
        return $this->belongsTo('App\Service', 'service_id', 'id');
    }

    public function client() {
        return $this->belongsTo('App\Client', 'client_id', 'id');
    }

    public function decorate(){
        return new \App\Decorators\ContractDecorator($this);
    }
}
