<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class EnterpriseService extends Pivot 
{

    protected $table = 'enterprise_service';
    public $timestamps = false;

}