<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model {

    protected $table = 'projects';
    public $timestamps = true;
    
    protected $with = ['contract'];
    
    public function tasks() {
        return $this->hasMany('App\Project', 'project_id', 'id');
    }

    public function contract() {
        return $this->belongsTo('App\Contract', 'contract_id', 'id');
    }

    
    public function decorate(){
        return new \App\Decorators\ProjectDecorator($this);
    }
}
