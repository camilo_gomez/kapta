<?php

namespace App\Decorators;

class EnterpriseDecorator extends Decorator {

    function name() {
        return $this->model->name;
    }

    function nameLink() {
        return view('components.html.button', [
            'text' => $this->name(),
            'class' => 'text-info',
            'link' => route('task.index' , [
                'enterprise_id' => $this->model->id
            ])
        ]);
    }

    

}
