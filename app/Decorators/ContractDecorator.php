<?php

namespace App\Decorators;

use App\Contract;
use App\Project;

class ContractDecorator extends Decorator {

    function enterprise() {
        return $this->model->enterprise->decorate()->nameLink();
    }

    function client() {
        return $this->model->client->decorate()->nameLink();
    }

    function service() {
        return $this->model->service->decorate()->nameLink();
    }

    
      function status() {

        if ($this->model->status == Contract::STATUS_IN_PROGRESS) {
            $options = [
                (object) [
                    'text' => "Finalizar Contrato",
                    'link' => route('contract.changeStatus', [
                        'contract' => $this->model,
                        'status' => Contract::STATUS_ENDED,
                    ])
                ]
            ];
            return view('components.html.dropdown', [
                'text' => "En progreso",
                'class' => 'btn btn-success ',
                'options' => $options,
            ]);
        }
       if ($this->model->status == Contract::STATUS_ENDED) {
            $options = [
                (object) [
                    'text' => "Reanudar Contrato",
                    'link' => route('contract.changeStatus', [
                        'contract' => $this->model,
                        'status' => Contract::STATUS_IN_PROGRESS,
                    ])
                ]
            ];
            return view('components.html.dropdown', [
                'text' => "Finalizado",
                'class' => 'btn btn-danger',
                'options' => $options,
            ]);
        }

        return "Error";
    }
    
    
    public function select(Project $project){
        
         return view('components.html.button', [
                'text' => "Seleccionar",
                'class' => 'btn btn-success',
                'link' => route('project.setContract' , [
                    'project' =>$project,
                    'contract' => $this->model
                ]),
            ]);
    }
    
}
