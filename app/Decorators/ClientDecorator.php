<?php

namespace App\Decorators;

class ClientDecorator extends Decorator {

    function name() {
        return $this->model->name;
    }

    function nameLink() {
        return $this->name();
        return view('components.html.button', [
            'text' => $this->name(),
            'class' => 'text-info',
            'link' => route('client.index')
        ]);
    }

    

}
