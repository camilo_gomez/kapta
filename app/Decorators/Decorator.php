<?php

namespace App\Decorators;

use Illuminate\Database\Eloquent\Model;

class Decorator {
    
    public $model;
    
    function __construct(Model $model) {
        $this->model = $model;
    }
    
}
