<?php

namespace App\Decorators;

class ServiceDecorator extends Decorator {

    function name() {
        return $this->model->name;
    }

    function nameLink() {
         return $this->model->name;
        return view('components.html.button', [
            'text' => $this->name(),
            'class' => 'text-info',
            'link' => route('service.index')
        ]);
    }

    

}
