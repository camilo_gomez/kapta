<?php

namespace App\Decorators;

use App\Task;

class TaskDecorator extends Decorator {

    function name() {
        return $this->model->title;
    }

    function nameLink() {
         return $this->model->title;
        return view('components.html.button', [
            'text' => $this->name(),
            'class' => 'text-info',
            'link' => route('task.index')
        ]);
    }

    function freedcamp() {
        if ($this->model->fc_id != null) {
            $fc_id = $this->model->fc_id;
            return view('components.html.button', [
                'text' => "FreedCamp",
                'class' => 'text-info',
                'link' => "https://freedcamp.com/caegomezji_RSn/Implementacin_y__Jzo/todos/$fc_id"
            ]);
        }
    }

    function project() {
        if ($this->model->project != null) {
            return $this->model->project->decorate()->nameLink();
        }
        return "";
    }

    function priority($priority = -99) {
        
        if($priority == -99){
            $priority = $this->model->priority;
        }
        
        if ($priority == Task::PRIORITY_NONE) {
            return view('components.html.p', [
                'text' => "",
                'class' => '',
            ]);
        }
        if ($priority == Task::PRIORITY_LOW) {
            return view('components.html.p', [
                'text' => "Baja",
                'class' => '',
            ]);
        }
        if ($priority == Task::PRIORITY_MEDIUM) {
            return view('components.html.p', [
                'text' => "Media",
                'class' => '',
            ]);
        }
        if ($priority == Task::PRIORITY_HIGH) {
            return view('components.html.p', [
                'text' => "Alta",
                'class' => '',
            ]);
        }

        return "error";
    }

    function assigned_to() {
        if ($this->model->assignedTo != null) {
            return $this->model->assignedTo->decorate()->nameLink();
        }
        return "";
    }

    function status() {

        $options = [
            (object) [
                'text' => "No empezada",
                'link' => route('task.changeStatus', [
                    'task' => $this->model,
                    'status' => Task::STATUS_NOT_STARTED,
                ])
            ],
            (object) [
                'text' => "En Progreso",
                'link' => route('task.changeStatus', [
                    'task' => $this->model,
                    'status' => Task::STATUS_IN_PROGRESS,
                ])
            ],
            (object) [
                'text' => "Terminada",
                'link' => route('task.changeStatus', [
                    'task' => $this->model,
                    'status' => Task::STATUS_COMPLETED,
                ])
            ],
        ];

        if ($this->model->status == Task::STATUS_COMPLETED) {

            return view('components.html.dropdown', [
                'text' => "Completada",
                'class' => 'btn btn-success ',
                'options' => $options,
            ]);
        }

        if ($this->model->status == Task::STATUS_IN_PROGRESS) {

            return view('components.html.dropdown', [
                'text' => "En Progreso",
                'class' => 'btn btn-warning ',
                'options' => $options,
            ]);
        }

        if ($this->model->status == Task::STATUS_NOT_STARTED) {

            return view('components.html.dropdown', [
                'text' => "No empezada",
                'class' => 'btn btn-danger ',
                'options' => $options,
            ]);
        }
    }

}
