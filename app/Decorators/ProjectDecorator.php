<?php

namespace App\Decorators;

class ProjectDecorator extends Decorator {

    function name() {
        return $this->model->title;
    }

    function nameLink() {
        return view('components.html.button', [
            'text' => $this->name(),
            'class' => 'text-info',
            'link' => route('task.index' , [
                'project_id' => $this->model->id
            ])
        ]);
    }
    
    function enterprise(){
        if($this->model->contract != null){
            return $this->model->contract->decorate()->enterprise();
        }
        return "";
    }

    function service(){
        if($this->model->contract != null){
            return $this->model->contract->decorate()->service();
        }
        return "";
    }
    function client(){
        if($this->model->contract != null){
            return $this->model->contract->decorate()->client();
        }
        return "";
    }
    
    
    function synchronize(){
        if($this->model->contract != null){
            return "";
        }
         return view('components.html.button', [
            'text' => "Asignar.. ",
            'class' => 'btn btn-sm btn-warning',
            'link' => route('project.show', [
                'project' => $this->model
            ])
        ]);
    }

}
