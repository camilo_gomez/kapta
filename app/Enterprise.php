<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enterprise extends Model 
{

    protected $table = 'enterprises';
    public $timestamps = true;
    
    
   
    public function contracts()
    {
        return $this->hasMany('App\Contract', 'enterprise_id', 'id');
    }

    public function services()
    {
        return $this->belongsToMany('App\Service')->using('App\EnterpriseService');
    }
    
    public function decorate(){
        return new \App\Decorators\EnterpriseDecorator($this);
    }

}