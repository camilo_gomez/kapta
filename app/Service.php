<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model 
{

    protected $table = 'services';
    public $timestamps = true;

    
    public function enterprises()
    {
        return $this->belongsToMany('App\Enterprise')->using('App\EnterpriseService');
    }

    
    public function decorate(){
        return new \App\Decorators\ServiceDecorator($this);
    }
}