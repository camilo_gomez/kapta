<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model 
{

    protected $table = 'clients';
    public $timestamps = true;

    
    public function contracts()
    {
        return $this->hasMany('App\Contract', 'cient_id', 'id');
    }

    public function decorate(){
        return new \App\Decorators\ClientDecorator($this);
    }
    
}