<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model {

    const STATUS_NOT_STARTED = 0;
    const STATUS_COMPLETED = 1;
    const STATUS_IN_PROGRESS = 2;
    const PRIORITY_NONE = 0;
    const PRIORITY_LOW = 1;
    const PRIORITY_MEDIUM = 2;
    const PRIORITY_HIGH = 3;
    
    const PRIORITY_ARRAY = [
        self::PRIORITY_NONE,
        self::PRIORITY_LOW,
        self::PRIORITY_MEDIUM,
        self::PRIORITY_HIGH,
    ];

    protected $table = 'tasks';
    public $timestamps = true;
    protected $with = ['project', 'assignedTo'];

    public function project() {
        return $this->belongsTo('App\Project', 'project_id', 'id');
    }

    public function assignedTo() {
        return $this->belongsTo('App\User', 'assigned_to', 'id');
    }

    public function decorate() {
        return new \App\Decorators\TaskDecorator($this);
    }

}
