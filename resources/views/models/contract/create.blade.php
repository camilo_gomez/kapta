@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="cabecera"> Crear un Contrato Nuevo </div>

                <div class="card-body text-center">

                    <form method="POST" action="{{ route('contract.store') }}"  enctype="multipart/form-data">
                        @csrf


                        <div 
                            class="form-group row"
                            >
                            <label 
                                for="enterprise" 
                                class="col-md-4 col-form-label text-md-right">
                                {{ __('Proyecto') }}
                            </label>
                            <div 
                                class="col-md-6"
                                >
                                <select 
                                    class="custom-select  @error('enterprise') is-invalid @enderror"
                                    id="enterprise" 
                                    name="enterprise"
                                    required>

                                    <option></option>
                                    
                                    @foreach($enterprises as $enterprise)
                                        <option
                                            value = "{{$enterprise->id}}"
                                            >{{$enterprise->decorate()->name()}}</option>
                                    @endforeach
                                    
                                </select>
                            </div>
                            @error('enterprise')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $enterprise }}</strong>
                            </span>
                            @enderror

                        </div>

                        
                        
                        
                        
                        <div 
                            class="form-group row"
                            >
                            <label 
                                for="client" 
                                class="col-md-4 col-form-label text-md-right">
                                {{ __('Cliente') }}
                            </label>
                            <div 
                                class="col-md-6"
                                >
                                <select 
                                    class="custom-select  @error('client') is-invalid @enderror"
                                    id="client" 
                                    name="client"
                                    required>

                                    <option></option>
                                    
                                    @foreach($clients as $client)
                                        <option
                                            value = "{{$client->id}}"
                                            >{{$client->decorate()->name()}}</option>
                                    @endforeach
                                    
                                </select>
                            </div>
                            @error('client')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $client }}</strong>
                            </span>
                            @enderror

                        </div>
                        
                        
                        
                         <div 
                            class="form-group row"
                            >
                            <label 
                                for="service" 
                                class="col-md-4 col-form-label text-md-right">
                                {{ __('Servicio que se ofrece') }}
                            </label>
                            <div 
                                class="col-md-6"
                                >
                                <select 
                                    class="custom-select  @error('service') is-invalid @enderror"
                                    id="service" 
                                    name="service"
                                    required>

                                    <option></option>
                                    
                                    @foreach($services as $service)
                                        <option
                                              value = "{{$service->id}}"
                                            >{{$service->decorate()->name()}}</option>
                                    @endforeach
                                    
                                </select>
                            </div>
                            @error('service')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $service }}</strong>
                            </span>
                            @enderror

                        </div>
                        
                        
                        
                        <div 
                            class="form-group row mb-0"
                            >
                            <div 
                                class="col-md-6 offset-md-4"
                                >
                                <button 
                                    type="submit" 
                                    class="btn btn-primary"
                                    >
                                    {{ __('Crear') }}
                                </button>
                            </div>
                        </div>


                    </form>



                </div>
            </div>
        </div>
    </div>
</div>
@endsection
