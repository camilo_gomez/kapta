@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="cabecera">                    
                    <h4>
                        Contratos
                    </h4>
                
                </div>
                
                <div class="card-body ">
                    
                    <p>
                        Tabla con los contratos existentes.  
                    </p>
                    
                    <a 
                        class='btn btn-warning my-3'
                        href="{{route('contract.create')}}"
                        >
                           Crear contrato
                    </a>
                    
                   @include('models.contract.index')


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
