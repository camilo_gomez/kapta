

<table class='table table-sm table-striped ' >
    <thead class='thead-dark '>
        <tr>
            <th>
                id
            </th>

            <th>
                Empresa
            </th>

            <th>
                Cliente
            </th>

            <th>
                Servicio
            </th>

            <th>
                Estado
            </th>

            <th>
                Fecha Creacion
            </th>


            <th>
                Fecha Actualizacion
            </th>


        </tr>
    </thead>
    <tbody>
        @foreach ($contracts as $contract )

        <tr>
            <th>
                {{$contract->id}}
            </th>

            <th>                                        
                {{$contract->decorate()->enterprise()}}
            </th>

            <th>
                {{$contract->decorate()->client()}}
            </th>

            <th>
                {{$contract->decorate()->service()}}
            </th>

            <th>
                {{$contract->decorate()->status()}}
            </th>

            <th>
                {{$contract->created_at}}
            </th>

            <th>
                {{$contract->updated_at}}
            </th>                                   


        </tr>

        @endforeach
    </tbody>
</table>

