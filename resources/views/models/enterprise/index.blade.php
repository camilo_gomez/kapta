
                        <table class='table table-sm table-striped ' >
                            <thead class='thead-dark '>
                                <tr>
                                    <th>
                                        id
                                    </th>
                                    <th>
                                        Nombre
                                    </th>
                                    <th>
                                        Fecha Creacion
                                    </th>
                                     <th>
                                        Fecha Actualizacion
                                    </th>
                                  
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($enterprises as  $enterprise)

                                  <tr>
                                     <th>
                                        {{$enterprise->id}}
                                    </th>
                                    <th>
                                       {{ $enterprise->decorate()->nameLink()}}
                                    </th>
                                    <th>
                                       {{ $enterprise->created_at}}
                                    </th>
                                     <th>
                                       {{$enterprise->updated_at}}
                                    </th>
                                                                     
                                    
                                </tr>


                                @endforeach
                            </tbody>
                        </table>



