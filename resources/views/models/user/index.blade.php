<table class='table table-sm table-striped ' >
    <thead class='thead-dark '>
        <tr>
            <th>
                id
            </th>
            <th>
                Nombre
            </th>
            <th>
                Email
            </th>
            <th>
                FreedCamp
            </th>

            <th>
                Fecha Creacion
            </th>
            <th>
                Fecha Actualizacion
            </th>

        </tr>
    </thead>
    <tbody>
        @foreach ( $users as $user )

        <tr>
            <th>
                {{$user->id}}
            </th>
            <th>
                {{ $user->decorate()->nameLink()}}
            </th>
            <th>
                {{ $user->email}}
            </th>
            <th>
                {{ $user->fc_id}}
            </th>
            <th>
                {{ $user->created_at}}
            </th>
            <th>
                {{$user->updated_at}}
            </th>


        </tr>

        @endforeach
    </tbody>
</table>



