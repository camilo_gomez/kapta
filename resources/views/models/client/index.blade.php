<table class='table table-sm table-striped ' >
    <thead class='thead-dark '>
        <tr>
            <th>
                id
            </th>
            <th>
                Nombre
            </th>
            <th>
                Fecha Creacion
            </th>
            <th>
                Fecha Actualizacion
            </th>

        </tr>
    </thead>
    <tbody>
        @foreach ( $clients as $client )

        <tr>
            <th>
                {{$client->id}}
            </th>
            <th>
                {{ $client->decorate()->nameLink()}}
            </th>
            <th>
                {{ $client->created_at}}
            </th>
            <th>
                {{$client->updated_at}}
            </th>


        </tr>

        @endforeach
    </tbody>
</table>
