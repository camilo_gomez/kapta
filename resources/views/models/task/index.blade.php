

                    <table class='table table-sm table-striped ' >
                        <thead class='thead-dark '>
                            <tr>
                                <th>
                                    id
                                </th>
                                <th>
                                    Nombre
                                </th>
                               
                                <th>
                                    Proyecto
                                </th>
                                <th>
                                    Prioridad
                                </th>
                                <th>
                                    Encargado
                                </th>
                                <th>
                                    Estado
                                </th>
                                <th>
                                    Fecha de incio
                                </th>
                                <th>
                                    Fecha de finalizacion
                                </th>

                                <th>
                                    Fecha Creacion
                                </th>

                            </tr>
                        </thead>
                        <tbody>
                            @foreach ( $tasks as $task )

                            <tr>
                                <th>
                                    {{$task->id}}
                                </th>
                                <th>
                                    {{ $task->decorate()->nameLink()}}
                                </th>
                              
                                <th>
                                    {{ $task->decorate()->project()}}
                                </th>
                                <th>
                                    {{ $task->decorate()->priority()}}
                                </th>
                                <th>
                                    {{ $task->decorate()->assigned_to()}}
                                </th>
                                <th>
                                    {{ $task->decorate()->status()}}
                                </th>
                                <th>
                                    {{ $task->started_at}}
                                </th>
                                <th>
                                    {{ $task->finished_at}}
                                </th>

                                <th>
                                    {{ $task->created_at}}
                                </th>


                            </tr>

                            @endforeach
                        </tbody>
                    </table>


