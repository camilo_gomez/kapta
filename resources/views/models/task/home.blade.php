@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="cabecera"> 
                    
                     Tareas
                
                </div>
                
               
                
                <div class="card-body ">
                    
                    
                     <p>
                        Tabla con las tareas existentes.  
                    </p>
                    
                    <a 
                        class='btn btn-warning my-3'
                        href="{{route('task.create')}}"
                        >
                           Crear Tarea
                    </a>

                   @include('models.task.index')



                </div>
            </div>
        </div>
    </div>
</div>
@endsection
