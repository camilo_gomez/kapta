@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="cabecera"> Crear un Servicio Nuevo </div>

                <div class="card-body text-center">

                    <form method="POST" action="{{ route('task.store') }}"  enctype="multipart/form-data">
                        @csrf


                        <div 
                            class="form-group row"
                            >
                            <label 
                                for="name" 
                                class="col-md-4 col-form-label text-md-right">
                                {{ __('Nombre') }}
                            </label>
                            <div 
                                class="col-md-6"
                                >

                                <input id="name" 
                                       type="text"
                                       class="form-control @error('name') is-invalid @enderror"
                                       name="name" 
                                       value="{{ old('name') }}"
                                       required 
                                       autocomplete="name" 
                                       autofocus>

                                @error('name')
                                <span 
                                    class="invalid-feedback" 
                                    role="alert">
                                    <strong>
                                        {{ $message }}
                                    </strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        
                        
                        
                        
                          <div 
                            class="form-group row"
                            >
                            <label 
                                for="project" 
                                class="col-md-4 col-form-label text-md-right">
                                {{ __('Empresa que ofrece el servicio') }}
                            </label>
                            <div 
                                class="col-md-6"
                                >
                                <select 
                                    class="custom-select  @error('project') is-invalid @enderror"
                                    id="project" 
                                    name="project"
                                    required>

                                    <option></option>
                                    
                                    @foreach($projects as $project)
                                        <option
                                            value = "{{$project->id}}"
                                            >{{$project->decorate()->name()}}</option>
                                    @endforeach
                                    
                                </select>
                            </div>
                            @error('project')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $project }}</strong>
                            </span>
                            @enderror

                        </div>
                        
                        
                        
                         <div 
                            class="form-group row"
                            >
                            <label 
                                for="priority" 
                                class="col-md-4 col-form-label text-md-right">
                                {{ __('Prioridad de la tarea') }}
                            </label>
                            <div 
                                class="col-md-6"
                                >
                                <select 
                                    class="custom-select  @error('priority') is-invalid @enderror"
                                    id="priority" 
                                    name="priority"
                                    >
                                    
                                    @foreach(\App\Task::PRIORITY_ARRAY as $priority)
                                        <option
                                            value = "{{$priority}}"
                                            >{{(new \App\Task())->decorate()->priority($priority)}}</option>
                                    @endforeach
                                    
                                </select>
                            </div>
                            @error('priority')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $priority }}</strong>
                            </span>
                            @enderror

                        </div>
                        
                        
                        <div 
                            class="form-group row"
                            >
                            <label 
                                for="user" 
                                class="col-md-4 col-form-label text-md-right">
                                {{ __('Usuario  ') }}
                            </label>
                            <div 
                                class="col-md-6"
                                >
                                <select 
                                    class="custom-select  @error('user') is-invalid @enderror"
                                    id="user" 
                                    name="user"
                                    required>

                                    <option></option>
                                    
                                    @foreach($users as $user)
                                        <option
                                            value = "{{$user->id}}"
                                            >{{$user->decorate()->name()}}</option>
                                    @endforeach
                                    
                                </select>
                            </div>
                            @error('user')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $user }}</strong>
                            </span>
                            @enderror

                        </div>
                        
                        
                        
                        
                        <div 
                            class="form-group row mb-0"
                            >
                            <div 
                                class="col-md-6 offset-md-4"
                                >
                                <button 
                                    type="submit" 
                                    class="btn btn-primary"
                                    >
                                    {{ __('Crear') }}
                                </button>
                            </div>
                        </div>


                    </form>



                </div>
            </div>
        </div>
    </div>
</div>
@endsection
