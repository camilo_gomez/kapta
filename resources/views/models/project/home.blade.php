@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="cabecera"> 

                    Proyectos

                </div>


                <div class="card-body text-center">
                    <p>
                        Listado de todos los proyectos en FreedCamp
                    </p>
                    
                    <a href="{{route('sychronize')}}"
                       class ='btn btn-warning my-3'
                       >
                        Sincronizar con FreedCamp
                    </a>
                       

                    @include('models.project.index')



                </div>
            </div>
        </div>
    </div>
</div>
@endsection
