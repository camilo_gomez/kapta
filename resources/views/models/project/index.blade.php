
<table class='table table-sm table-striped ' >
    <thead class='thead-dark '>
        <tr>
            <th>
                id
            </th>
            <th>
                Titulo
            </th>
            <th>
                Descripción
            </th>
            <th>
                Empresa
            </th>

            <th>
                Cliente
            </th>

            <th>
                Servicio
            </th>
            <th>
                FreedCamp link
            </th>
            <th>
                Fecha creación
            </th>
            <th>

            </th>          
        </tr>
    </thead>
    <tbody>
        @foreach ( $projects as  $project)

        <tr>
            <th>
                {{$project->id}}
            </th>
            <th>
                {{$project->decorate()->nameLink()}}
            </th>
            <th>
                {{$project->description}}
            </th>

            <th>
                {{$project->decorate()->enterprise()}}
            </th>

            <th>
                {{$project->decorate()->client()}}
            </th>

            <th>
                {{$project->decorate()->service()}}
            </th>

            <th>
                {{$project->fc_id}}
            </th>
            <th>
                {{$project->created_at}}
            </th>
            <th>
                {{$project->decorate()->synchronize()}}
            </th>


        </tr>

        @endforeach
    </tbody>
</table>



