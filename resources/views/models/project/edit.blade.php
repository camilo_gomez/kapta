@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="cabecera"> Configuracion de {{$project->decorate()->name()}}</div>

                <div class="card-body text-center">
                    <p>
                        Selecciona la configuración que corresponde al proyecto  {{$project->decorate()->name()}}
                    </p>


                    <table class='table table-sm table-striped ' >
                        <thead class='thead-dark '>
                            <tr>
                                <th>
                                    id
                                </th>

                                <th>
                                    Empresa
                                </th>

                                <th>
                                    Cliente
                                </th>

                                <th>
                                    Servicio
                                </th>

                                <th>
                                    
                                </th>


                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($contracts as $contract )

                            <tr>
                                <th>
                                    {{$contract->id}}
                                </th>

                                <th>                                        
                                    {{$contract->decorate()->enterprise()}}
                                </th>

                                <th>
                                    {{$contract->decorate()->client()}}
                                </th>

                                <th>
                                    {{$contract->decorate()->service()}}
                                </th>

                                <th>
                                    {{$contract->decorate()->select($project)}}
                                </th>

                                                               


                            </tr>

                            @endforeach
                        </tbody>
                    </table>






                </div>
            </div>
        </div>
    </div>
</div>
@endsection
