
                        <table class='table table-sm table-striped ' >
                            <thead class='thead-dark '>
                                <tr>
                                    <th>
                                        id
                                    </th>
                                    <th>
                                        Nombre
                                    </th>
                                    <th>
                                        Fecha Creacion
                                    </th>
                                     <th>
                                        Fecha Actualizacion
                                    </th>
                                  
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ( $services as $service )

                                <tr>
                                     <th>
                                        {{$service->id}}
                                    </th>
                                    <th>
                                       {{ $service->decorate()->nameLink()}}
                                    </th>
                                    <th>
                                       {{ $service->created_at}}
                                    </th>
                                     <th>
                                       {{$service->updated_at}}
                                    </th>
                                                                     
                                    
                                </tr>

                                @endforeach
                            </tbody>
                        </table>


