@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="cabecera"> 
                    
                     Servicios
                
                </div>
                
               
                <div class="card-body">
                    
                     <p>
                        Tabla con los Servicios existentes.  
                    </p>
                    
                    <a 
                        class='btn btn-warning my-3'
                        href="{{route('service.create')}}"
                        >
                           Crear Servicio
                    </a>
                

                   @include('models.service.index')



                </div>
            </div>
        </div>
    </div>
</div>
@endsection
