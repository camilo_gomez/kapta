<a 
    class="{{$class ?? ''}}"  
    href ="{{$link  ?? ''}}"
    
    @isset($id)
    id="{{$id ?? ''}}"
    @endif
    
    {{$attributes ?? ''}}    
    
    
    >
    {{$text ?? '' }}
</a>