<p 
    class="{{$class ?? ''}}"  
    
    @isset($id)
    id="{{$id ?? ''}}"
    @endif
    
    >
    {{$text ?? '' }}
</p>