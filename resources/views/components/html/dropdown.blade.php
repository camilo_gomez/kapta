<div class="btn-group">
    <button 
        type="button" 
        class="{{$class ?? ''}}  dropdown-toggle"   
        data-toggle="dropdown" 
        aria-haspopup="true" 
        aria-expanded="false"
        >
        {{$text ?? '' }}
    </button>
    <div class="dropdown-menu">

        @isset($options)
        @foreach($options as $op)
        <a 
            class="dropdown-item" 

            @isset($op->link)
            href="{{$op->link}}"
            @endif          
            >
            @isset($op->text)
            {{$op->text}}
            @endif              
        </a>

        @endforeach
        @endif



    </div>
</div>