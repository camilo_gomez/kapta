<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

/*
  DB::listen(function ($query){
  echo "<pre> {$query->sql} </pre>";
  });
 */

Route::get('/reset', function () {
    echo Artisan::call("migrate:refresh");
    echo Artisan::call("db:seed");
    return "PAGINA RESETEADA";
});

Route::get('/', function (Illuminate\Http\Request $request) {
    //dd($request->session()->all());
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

 Route::get('/sychronize', 'SychronizeController@synchronize')->name('sychronize');
 Route::get('/api/projects', 'FreedCampAPI\ProjectsAPIController@index')->name('api.projects');
 Route::get('/api/task', 'FreedCampAPI\TaskAPIController@index')->name('api.task');

Route::middleware('auth')->namespace('Resource')->group(function() {
    
   
    
    Route::resource('project', 'ProjectController');
    Route::resource('contract', 'ContractController');
    Route::resource('enterprise', 'EnterpriseController');
    Route::resource('task', 'TaskController');
    Route::resource('enterpriseservice', 'EnterpriseServiceController');
    Route::resource('client', 'ClientController');
    Route::resource('service', 'ServiceController');
    Route::resource('user', 'UserController');
    
    
    Route::GET('contract/{contract}/changeStatus/{status}' , 'ContractController@changeStatus')
            ->name('contract.changeStatus');
    
    Route::GET('task/{task}/changeStatus/{status}' , 'TaskController@changeStatus')
            ->name('task.changeStatus');
    
    Route::GET('project/{project}/setContract/{contract}' , 'ProjectController@setContract')
            ->name('project.setContract');
    
});




